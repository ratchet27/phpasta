ifneq (,$(wildcard ./.env))
    include .env
    export
endif

# Запустить установку проекта

install:
	docker volume create --name=${APP_NAME}-database-volume
	docker-compose up -d --build --force-recreate --remove-orphans
	docker exec -it ${APP_NAME}-php composer update
	docker exec -it ${APP_NAME}-php php artisan key:generate
	docker exec -it ${APP_NAME}-php php artisan storage:link
	docker exec -it ${APP_NAME}-php php artisan migrate:fresh --seed

# Запустить Docker демона
run:
	docker-compose up -d
	docker exec -it ${APP_NAME}-php composer install
	docker exec -it ${APP_NAME}-php php artisan migrate:fresh --seed

#Запустить тесты
test:
	docker exec -it ${APP_NAME}-php ./vendor/bin/phpunit
# Остановить работу Docker'а
stop:
	docker-compose down -v

.PHONY: install run test stop