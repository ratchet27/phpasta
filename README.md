# Phpaste

## Description

This project provides API for text-sharing platform, inspired by [pastebin.com](https://pastebin.com). 

## Technological stack

The project is built using [Laravel framework](https://laravel.com)

## Documentation

The API collection is located in [postman collection](docs/postman.json). You're also need to specify Postman environment with DOMAIN variable, such as `localhost` or `phpaste.test` etc.

## Installation

### Docker example 
1) clone project
2) `cp .env.example .env`
3) setup your environment variables in `.env` file
4) Make sure you have docker service installed
5) run `make install` for initial installation and container building
6) `make stop` for stopping docker containers
7) `make run` for running after initial installation have been done

### Vagrant example
#### Requirements
`php >= 8.0` 

`postgresql 12`

1) clone project
2) `cp .env.example .env`
3) setup your environment variables in `.env` file
4) install dependencies `composer i`
5) generate application encryption key `php artisan key:generate`
6) run database migrations with initial seeding `php artisan migrate --seed`


You can start using project now.
Admin routes are available with Bearer token requests. 
Token can be acquired with default admin account : login `admin`, password `password`

## Testing

Run `./vendor/bin/phpunit` or configure your IDE PHPUnit integration