<?php

declare(strict_types=1);

use App\Http\Controllers\Admin\PasteController as AdminPasteController;
use App\Http\Controllers\User\PasteController as UserPasteController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function (Router $router): void {
    $router->get('pastes', [UserPasteController::class, 'list']);
    $router->get('pastes/{paste}', [UserPasteController::class, 'show']);

    $router->post('pastes', [UserPasteController::class, 'addPaste']);

    $router->group(['prefix' => 'admin'], function (Router $router): void {
        $router->post('login', [AuthController::class, 'login']);

        $router->group(['middleware' => 'auth:sanctum'], function (Router $router): void {
            $router->get('logout', [AuthController::class, 'logout']);

            $router->get('pastes', [AdminPasteController::class, 'list']);
            $router->post('pastes/{paste}', [AdminPasteController::class, 'updatePaste']);
            $router->delete('pastes/{paste}', [AdminPasteController::class, 'deletePaste']);
        });
    });
});
