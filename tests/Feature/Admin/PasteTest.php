<?php

namespace Tests\Feature\Admin;

use App\Models\Paste;
use Tests\TestCase;

final class PasteTest extends TestCase
{
    /** @test */
    public function user_can_search_private_pastes(): void
    {
        $this->addPublicPaste();
        $this->addPrivatePaste();

        Paste::factory()->create([
            'title'     => 'Some orger words',
            'content'   => 'some text',
            'is_public' => true,
            'uuid'      => '8e3ceb4b-edf3-4286-82ae-6777e78d99f8',
            'lifetime'  => null,
            'syntax'    => null,
        ]);

        $response = $this->asUser()->getJson('api/v1/admin/pastes?page=1&perPage=15&search=paste&showPrivate=1');

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'data' => [
                [
                    'title'    => 'Public paste',
                    'content'  => 'some text',
                    'isPublic' => true,
                    'uuid'     => 'e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5',
                    'lifetime' => null,
                    'syntax'   => null,
                ],
                [
                    'title'    => 'Private paste',
                    'content'  => 'some paste text',
                    'isPublic' => false,
                    'uuid'     => 'd5d9bf4c-d748-4f57-872f-326af673c2f5',
                    'lifetime' => null,
                    'syntax'   => null,
                ],
            ],
        ]);
        $response->assertJsonMissing([
            'data' => [
                [
                    'title'     => 'Some orger words',
                    'content'   => 'some text',
                    'is_public' => true,
                    'uuid'      => '8e3ceb4b-edf3-4286-82ae-6777e78d99f8',
                    'lifetime'  => null,
                    'syntax'    => null,
                ],
            ],
        ]);
    }

    /** @test */
    public function user_can_update_paste(): void
    {
        $this->addPublicPaste();

        $response = $this->asUser()->postJson('/api/v1/admin/pastes/e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5', [
            'title'    => 'new title',
            'content'  => 'new content',
            'isPublic' => false,
        ]);

        $response->assertJson(['uuid' => 'e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5']);

        $this->assertDatabaseHas('pastes', [
            'title'     => 'new title',
            'content'   => 'new content',
            'is_public' => false,
            'uuid'      => 'e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5',
        ]);
    }

    /** @test */
    public function user_can_soft_delete_paste(): void
    {
        $this->addPublicPaste();

        $response = $this->asUser()->deleteJson('/api/v1/admin/pastes/e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5');

        $response->assertJson(['message' => 'paste deleted']);

        $this->assertSoftDeleted('pastes', [
            'uuid' => 'e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5',
        ]);
    }

}