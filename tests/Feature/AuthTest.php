<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

final class AuthTest extends TestCase
{
    /** @test */
    public function user_can_login(): void
    {
        User::factory()->create(['login' => 'admin']);

        $response = $this->postJson('api/v1/admin/login', ['login' => 'admin', 'password' => 'password']);

        $response->assertStatus(200);
        $response->assertJsonStructure(['token']);
    }

    /** @test */
    public function only_logged_user_can_logout(): void
    {
        $wrongResponse = $this->getJson('api/v1/admin/logout');
        $wrongResponse->assertStatus(401);

        $user = User::factory()->create(['login' => 'admin']);

        $response = $this->actingAs($user)->getJson('api/v1/admin/logout');

        $response->assertStatus(200);
    }
}