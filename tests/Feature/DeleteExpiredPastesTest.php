<?php

namespace Tests\Feature;

use App\Models\Paste;
use Carbon\Carbon;
use Tests\TestCase;

final class DeleteExpiredPastesTest extends TestCase
{
    /** @test */
    public function expired_pastes_can_be_deleted(): void
    {
        Carbon::setTestNow('2021-01-01 00:00:00');
        Paste::factory()->create([
            'title'     => 'Private paste',
            'content'   => 'some paste text',
            'is_public' => false,
            'uuid'      => 'd5d9bf4c-d748-4f57-872f-326af673c2f5',
            'lifetime'  => 60,
            'syntax'    => null,
        ]);

        Paste::factory()->create([
            'title'     => 'another private paste',
            'content'   => 'some paste text',
            'is_public' => false,
            'uuid'      => 'e16f388d-f4ab-4c7a-ab4d-6d487b68afe3',
            'lifetime'  => 100,
            'syntax'    => null,
        ]);

        $this->assertDatabaseHas('pastes', ['uuid' => 'd5d9bf4c-d748-4f57-872f-326af673c2f5']);
        $this->assertDatabaseHas('pastes', ['uuid' => 'e16f388d-f4ab-4c7a-ab4d-6d487b68afe3']);

        Carbon::setTestNow('2021-01-01 00:01:00');

        $this->artisan('paste:delete-expired')->assertExitCode(0);

        $this->assertSoftDeleted('pastes', ['uuid' => 'd5d9bf4c-d748-4f57-872f-326af673c2f5']);
        $this->assertNotSoftDeleted('pastes', ['uuid' => 'e16f388d-f4ab-4c7a-ab4d-6d487b68afe3']);


    }
}