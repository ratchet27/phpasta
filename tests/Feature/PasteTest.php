<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\Paste;
use Tests\TestCase;

final class PasteTest extends TestCase
{
    /** @test */
    public function cant_get_paste_list_without_params(): void
    {
        $response = $this->getJson('api/v1/pastes');

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'page',
                'perPage',
            ],
        ]);
    }

    /** @test */
    public function can_get_paste_list_with_proper_params(): void
    {
        Paste::factory()->count(10)->create();

        $response = $this->getJson('api/v1/pastes?page=1&perPage=15');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'links',
            'meta',
        ]);
    }

    /** @test */
    public function can_search_public_pastes(): void
    {
      $this->addPrivatePaste();
      $this->addPublicPaste();

        $response = $this->getJson('api/v1/pastes?page=1&perPage=15&search=paste');

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'data' => [
                [
                    'title'   => 'Public paste',
                    'content' => 'some text',
                    'uuid'    => 'e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5',
                ],
            ],
        ]);
        $response->assertJsonMissing([
            'data' => [
                [
                    'title'   => 'Private paste',
                    'content' => 'some text',
                    'uuid'    => 'd5d9bf4c-d748-4f57-872f-326af673c2f5',
                ],
            ],
        ]);
    }


    /** @test */
    public function can_add_a_new_paste(): void
    {
        $response = $this->postJson('api/v1/pastes', [
            'title'    => 'Test title',
            'content'  => 'Test content',
            'uuid'     => 'bdd572f0-9f68-4215-9889-0ace19f971bd',
            'isPublic' => true,
        ]);

        $response->assertStatus(200);
        $response->assertJson(['uuid' => 'bdd572f0-9f68-4215-9889-0ace19f971bd']);
    }

    /** @test */
    public function can_add_only_private_paste_with_lifetime(): void
    {
        $response = $this->postJson('api/v1/pastes', [
            'title'    => 'Test title',
            'content'  => 'Test content',
            'uuid'     => 'bdd572f0-9f68-4215-9889-0ace19f971bd',
            'isPublic' => true,
            'lifetime' => 1000,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors'  => [
                'lifetime' => [
                    'Only private pastes can be deleted after given time',
                ],
            ],
        ]);
    }
}