<?php

namespace Tests;

use App\Models\Paste;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    protected function addPrivatePaste(): Paste
    {
        return Paste::factory()->create([
            'title'     => 'Private paste',
            'content'   => 'some paste text',
            'is_public' => false,
            'uuid'      => 'd5d9bf4c-d748-4f57-872f-326af673c2f5',
            'lifetime' => null,
            'syntax' => null,
        ]);
    }


    protected function addPublicPaste(): Paste
    {
        return Paste::factory()->create([
            'title'     => 'Public paste',
            'content'   => 'some text',
            'is_public' => true,
            'uuid'      => 'e6cd1630-e1a2-4fd9-a0ab-26798cf5a2d5',
            'lifetime' => null,
            'syntax' => null,
        ]);
    }

    protected function asUser(): static
    {
        $user = User::factory()->create(['login' => 'admin']);
        return $this->actingAs($user);
    }
}
