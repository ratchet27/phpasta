<?php

declare(strict_types=1);

namespace App\DTO;

use Ramsey\Uuid\UuidInterface;

final class PasteDto
{

    public readonly string $title;
    public readonly string $content;
    public readonly UuidInterface $uuid;
    public readonly bool $isPublic;
    public readonly ?string $syntax;
    public readonly ?int $lifetime;

    public function __construct(string $title, string $content, UuidInterface $uuid, bool $isPublic, ?string $syntax, ?int $lifetime)
    {
        $this->title    = $title;
        $this->content  = $content;
        $this->uuid     = $uuid;
        $this->isPublic = $isPublic;
        $this->syntax   = $syntax;
        $this->lifetime = $lifetime;
    }
}