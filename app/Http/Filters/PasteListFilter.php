<?php

declare(strict_types=1);

namespace App\Http\Filters;

interface PasteListFilter
{
    public function getPage(): int;

    public function getPerPage(): int;

    public function getSearch(): ?string;

    public function showPrivate(): bool;

}