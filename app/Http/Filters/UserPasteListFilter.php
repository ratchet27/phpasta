<?php

declare(strict_types=1);

namespace App\Http\Filters;


final class UserPasteListFilter implements PasteListFilter
{
    private int $page;
    private int $perPage;
    private bool $showPrivate;
    private ?string $search;

    public function __construct(int $page, int $perPage, ?string $search)
    {
        $this->page = $page;
        $this->perPage = $perPage;
        $this->search = $search;
        $this->showPrivate = false;
    }

    public function showPrivate(): bool
    {
        return  $this->showPrivate;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return string|null
     */
    public function getSearch(): ?string
    {
        return $this->search;
    }

}