<?php

declare(strict_types=1);

namespace App\Http\Controllers\User;

use App\DTO\PasteDto;
use App\Events\PasteAddedEvent;
use App\Http\Filters\UserPasteListFilter;
use App\Http\Requests\Paste\AddPasteRequest;
use App\Http\Requests\Paste\User\ListPasteRequest;
use App\Http\Resources\Paste\PasteResource;
use App\Http\Resources\User\PasteListResource;
use App\Models\Paste;
use App\Services\PasteService;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Routing\Controller;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

final class PasteController extends Controller
{
    private PasteService $pasteService;
    private Dispatcher $dispatcher;
    private SerializerInterface $serializer;

    public function __construct(PasteService $pasteService, Dispatcher $dispatcher, SerializerInterface $serializer)
    {
        $this->pasteService = $pasteService;
        $this->dispatcher = $dispatcher;
        $this->serializer = $serializer;
    }

    public function list(ListPasteRequest $request): AnonymousResourceCollection
    {
        $filter = $this->serializer->denormalize($request->validated(), UserPasteListFilter::class, context: [
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
        ]);

        return PasteListResource::collection($this->pasteService->list($filter));
    }

    public function show(Paste $paste): PasteResource
    {
        return new PasteResource($paste);
    }

    public function addPaste(AddPasteRequest $request): JsonResponse
    {
        $dto = $this->serializer->denormalize($request->all(), PasteDto::class, context: [
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
        ]);

        $paste = $this->pasteService->addPaste($dto);

        $this->dispatcher->dispatch(new PasteAddedEvent($paste->uuid));

        return new JsonResponse(['uuid' => $paste->uuid]);
    }
}