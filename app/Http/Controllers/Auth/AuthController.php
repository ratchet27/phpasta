<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class AuthController extends Controller
{
    private AuthFactory $auth;

    public function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    public function login(LoginRequest $loginRequest): JsonResponse
    {
        if ($this->auth->guard()->attempt($loginRequest->only(['login', 'password']))) {
            /** @var User $user */
            $user = $this->auth->guard()->user();

            $user->tokens()->delete();

            return new JsonResponse(['token' => $user->createToken('phppaste-frontend')->plainTextToken]);
        }

        return new JsonResponse(['message' => 'Invalid credentials'], 401);

    }


    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();

        return new JsonResponse(['message' => 'Logged out']);
    }
}