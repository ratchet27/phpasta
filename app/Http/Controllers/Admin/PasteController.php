<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\DTO\PasteDto;
use App\Http\Filters\AdminPasteListFilter;
use App\Http\Requests\Paste\Admin\ListPasteRequest;
use App\Http\Requests\Paste\Admin\UpdatePasteRequest;
use App\Http\Resources\Admin\PasteListResource;
use App\Models\Paste;
use App\Services\PasteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Routing\Controller;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

final class PasteController extends Controller
{
    private PasteService $pasteService;
    private SerializerInterface $serializer;

    public function __construct(PasteService $pasteService, SerializerInterface $serializer)
    {
        $this->pasteService = $pasteService;
        $this->serializer = $serializer;
    }

    public function list(ListPasteRequest $request): AnonymousResourceCollection
    {
        /** @var AdminPasteListFilter $filter */
        $filter = $this->serializer->denormalize($request->validated(), AdminPasteListFilter::class, context: [
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
        ]);
        return PasteListResource::collection($this->pasteService->list($filter));
    }

    public function updatePaste(UpdatePasteRequest $request, Paste $paste) : JsonResponse
    {
        /** @var PasteDto $dto */
        $dto = $this->serializer->denormalize($request->all(), PasteDto::class);

        $this->pasteService->updatePaste($paste, $dto);

        return new JsonResponse(['uuid' => $paste->uuid]);
    }

    public function deletePaste(Paste $paste): JsonResponse
    {
        $paste->delete();

        return new JsonResponse(['message' => 'paste deleted']);
    }
}