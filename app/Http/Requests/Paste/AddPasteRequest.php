<?php

declare(strict_types=1);

namespace App\Http\Requests\Paste;

use App\Enums\PasteSyntax;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

final class AddPasteRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title'    => ['required', 'string'],
            'content'  => ['required', 'string'],
            'uuid'     => ['required', 'uuid', 'unique:pastes'],
            'isPublic' => ['required', 'boolean'],
            'syntax'   => ['nullable', 'string', new Enum(PasteSyntax::class)],
            'lifetime' => ['nullable', 'integer', 'min:1', 'prohibited_if:isPublic,true'],
        ];
    }

    protected function passedValidation(): void
    {
        $this->merge(['isPublic' => (bool) $this->input('isPublic')]);
    }
}