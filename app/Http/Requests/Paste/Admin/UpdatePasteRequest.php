<?php

declare(strict_types=1);

namespace App\Http\Requests\Paste\Admin;

use App\Enums\PasteSyntax;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

final class UpdatePasteRequest extends FormRequest
{
    public function prepareForValidation(): void
    {
        $this->merge([
            'uuid' => $this->route('paste')->uuid->toString(),
        ]);
    }

    public function rules(): array
    {
        return [
            'title'    => ['required', 'string'],
            'content'  => ['required', 'string'],
            'isPublic' => ['required', 'boolean'],
            'syntax'   => ['nullable', 'string', new Enum(PasteSyntax::class)],
            'lifetime' => ['nullable', 'integer', 'min:1', 'prohibited_if:isPublic,true'],
            'uuid'     => ['required', 'uuid'],
        ];
    }

    protected function passedValidation(): void
    {
        $this->merge(['isPublic' => (bool) $this->input('isPublic')]);
    }
}