<?php

declare(strict_types=1);

namespace App\Http\Requests\Paste\User;

use Illuminate\Foundation\Http\FormRequest;

final class ListPasteRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'page'         => ['required', 'int', 'min:1'],
            'perPage'      => ['required', 'int', 'min:1'],
            'search'       => ['nullable', 'string', 'min:3'],
        ];
    }
}