<?php

declare(strict_types=1);

namespace App\Http\Resources\Paste;

use App\Models\Paste;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/** @mixin Paste */
final class PasteResource extends JsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'title'     => $this->title,
            'content'   => $this->content,
            'syntax'    => $this->syntax,
        ];
    }
}
