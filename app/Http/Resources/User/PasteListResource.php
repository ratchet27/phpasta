<?php

declare(strict_types=1);

namespace App\Http\Resources\User;

use App\Models\Paste;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Paste */
final class PasteListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'uuid'    => $this->uuid,
            'title'   => $this->title,
            'content' => $this->content,
        ];
    }
}
