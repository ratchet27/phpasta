<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\ConnectionInterface;

final class DeleteExpiredPastesCommand extends Command
{
    protected $signature = 'paste:delete-expired';

    protected $description = 'Delete pastes with expired lifetimes';

    public function handle(ConnectionInterface $connection): void
    {
        $connection->table('pastes')
            ->whereNotNull('lifetime')
            ->whereRaw("(created_at + interval '1 second' * lifetime) <= ?",[now()])
            ->update(['deleted_at' => now()]);
    }
}
