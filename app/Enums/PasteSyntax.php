<?php

declare(strict_types=1);

namespace App\Enums;

enum PasteSyntax: string
{
    case JAVASCRIPT = 'javascript';
    case PHP = 'php';
    case C = 'c';
    case CPP = 'CPP';
    case HTML = 'html';
    case CSS = 'css';
}