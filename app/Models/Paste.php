<?php

declare(strict_types=1);

namespace App\Models;

use App\Casts\UuidCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\UuidInterface;

/**
 * @property string $title
 * @property string $content
 * @property UuidInterface $uuid
 * @property bool $is_public
 * @property string|null $syntax
 * @property int|null $lifetime
 */
final class Paste extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'content',
        'uuid',
        'is_public',
        'syntax',
        'lifetime',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'uuid' => UuidCast::class,
    ];

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    public function urlAttribute(): string
    {
        return $this->uuid->toString();
    }
}