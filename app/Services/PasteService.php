<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\PasteDto;
use App\Http\Filters\PasteListFilter;
use App\Models\Paste;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

final class PasteService
{
    public function list(PasteListFilter $filter): LengthAwarePaginator
    {
        return Paste::query()
            ->when(!$filter->showPrivate(), function (Builder $builder) : Builder {
                return $builder->where(['is_public' => true]);
            })
            ->when($filter->getSearch(), function (Builder $builder) use ($filter): Builder {
                return $builder->where(function (Builder $builder) use ($filter): Builder {
                    return $builder
                        ->where('title', 'ilike', "%{$filter->getSearch()}%")
                        ->orWhere('content', 'ilike', "%{$filter->getSearch()}%");
                });
            })
            ->paginate(perPage: $filter->getPerPage(), page: $filter->getPage());
    }

    public function addPaste(PasteDto $dto): Paste
    {
        $paste = new Paste();

        $paste->title     = $dto->title;
        $paste->content   = $dto->content;
        $paste->is_public = $dto->isPublic;
        $paste->syntax    = $dto->syntax;
        $paste->lifetime  = $dto->lifetime;

        $paste->uuid = $dto->uuid;

        $paste->save();

        return $paste;
    }

    public function updatePaste(Paste $paste, PasteDto $dto): Paste
    {
        $paste->title     = $dto->title;
        $paste->content   = $dto->content;
        $paste->is_public = $dto->isPublic;
        $paste->syntax    = $dto->syntax;
        $paste->lifetime  = $dto->lifetime;

        $paste->update();

        return $paste;
    }
}