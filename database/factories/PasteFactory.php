<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Enums\PasteSyntax;
use App\Models\Paste;
use Illuminate\Database\Eloquent\Factories\Factory;

final class PasteFactory extends Factory
{
    protected $model = Paste::class;

    public function definition(): array
    {
        return [
            'title'     => $this->faker->word,
            'content'   => $this->faker->sentence,
            'uuid'      => $this->faker->uuid,
            'is_public' => $isPublic = $this->faker->boolean,
            'syntax'    => $this->faker->randomElement(PasteSyntax::cases()),
            'lifetime'  => $isPublic ? null : $this->faker->randomNumber(),
        ];
    }
}
